SRCDIR=src
BUILDDIR=build
OBJDIR=obj
CC=gcc
CFLAGS=-g --std=c17 -Wall -pedantic -I$(SRCDIR)/ -ggdb -Wextra -Werror -DDEBUG

all: $(OBJDIR)/mem.o $(OBJDIR)/util.o $(OBJDIR)/mem_debug.o $(OBJDIR)/main.o $(OBJDIR)/test.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(OBJDIR)
	mkdir -p $(BUILDDIR)

$(OBJDIR)/%.o: $(SRCDIR)/%.c build
	$(CC) -c $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*
	
run: all
	$(BUILDDIR)/main

