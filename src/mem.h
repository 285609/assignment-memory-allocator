#ifndef _MEM_H_
#define _MEM_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/mman.h>

void* _malloc(const size_t query);
void  _free(void* mem);
void* heap_init(const size_t initial_size);

#define DEBUG_FIRST_BYTES 4

void debug_struct_info(FILE* f, void const* address);
void debug_heap(FILE* f, void const* ptr);

#endif
