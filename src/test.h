#ifndef _TEST_H_
#define _TEST_H_

#include <stdio.h>

#define TEST_COUNT 6

typedef void (test)(FILE* output);

void test_successful_alloc(FILE* output);

void test_free_one_block(FILE* output);

void test_free_two_blocks(FILE* output);

void test_merging_blocks_in_allocation(FILE* output);

void test_grow_heap_extending(FILE* output);

void test_grow_heap_not_extending(FILE* output);

static test* const tests[] = {
        test_successful_alloc,
        test_free_one_block,
        test_free_two_blocks,
        test_merging_blocks_in_allocation,
        test_grow_heap_extending,
        test_grow_heap_not_extending
};

#endif