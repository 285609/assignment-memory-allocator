#include "test.h"

#include <stdio.h>
#include <assert.h>
#include <sys/mman.h>
#include <inttypes.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_DEFAULT_SIZE (1024 * 1024)

struct test_block {
    block_size sz;
    block_capacity cap;
    struct block_header* block;
};

static const struct test_block empty_block = { 0 };

static void print_test_start(const char* test_name, FILE* output) {
	fprintf(output, "--- %s ---\n", test_name);
	fflush(output);
}

static void print_test_end(FILE* output) {
	fprintf(output, "--- Test passed! ---\n");
	fflush(output);
}

static int clear_heap(void* heap, size_t heap_size) {
    return munmap(heap, heap_size);
}

static struct test_block alloc_block(size_t query) {
	void* block_content = _malloc(query);
	if (!block_content) return empty_block;
    struct block_header* block_header = block_get_header(block_content);
    const block_capacity cap = block_header->capacity;
	return (struct test_block) {
        .cap = cap,
        .sz = size_from_capacity(cap),
        .block = block_header
    };
}

static void print_debug_heap(FILE* output, void* heap_start) {
	debug_heap(output, heap_start);
	fflush(output);
}

static void* test_start(const char* test_name, FILE* output) {
    print_test_start(test_name, output);
    void* heap_start = heap_init(HEAP_DEFAULT_SIZE);
    assert(heap_start != NULL);
    return heap_start;
}

static void test_end(FILE* output, void* heap_start) {
    clear_heap(heap_start, HEAP_DEFAULT_SIZE);
    print_test_end(output);
}

void test_successful_alloc(FILE* output) {
    void* heap_start = test_start("Test: Succesful allocation", output);

    const size_t first_block_cap = 1024;
	const struct test_block first_block = alloc_block(first_block_cap);
	print_debug_heap(output, heap_start);
	assert((void*) first_block.block == heap_start && first_block.cap.bytes == first_block_cap);

    const size_t second_block_cap = 5 * 1024;
    const struct test_block second_block = alloc_block(second_block_cap);
	print_debug_heap(output, heap_start);
	assert(second_block.block == first_block.block->next);
	assert((uint8_t*) first_block.block + first_block.sz.bytes == (uint8_t*) second_block.block);
		
	const block_size free_block_sz = { HEAP_DEFAULT_SIZE - first_block.sz.bytes - second_block.sz.bytes };
	assert(second_block.block->next->capacity.bytes == capacity_from_size(free_block_sz).bytes);

    test_end(output, heap_start);
}

void test_free_one_block(FILE* output) {
    void* heap_start = test_start("Test: Free one block", output);

    const size_t first_block_cap = 1024;
    const struct test_block first_block = alloc_block(first_block_cap);

    const size_t second_block_cap = 5 * 1024;
    const struct test_block second_block = alloc_block(second_block_cap);
    print_debug_heap(output, heap_start);

    _free(second_block.block->contents);
    print_debug_heap(output, heap_start);
    const struct block_header* free_block = first_block.block->next;
    assert(free_block->is_free && free_block->next == NULL);
    assert(size_from_capacity(free_block->capacity).bytes == HEAP_DEFAULT_SIZE - first_block.sz.bytes);

    test_end(output, heap_start);
}

void test_free_two_blocks(FILE* output) {
    void* heap_start = test_start("Test: Free two blocks", output);

    const size_t first_block_cap = 1024;
    const struct test_block first_block = alloc_block(first_block_cap);

    const size_t second_block_cap = 5 * 1024;
    const struct test_block second_block = alloc_block(second_block_cap);

    const size_t third_block_cap = 1000 * 1024;
    const struct test_block third_block = alloc_block(third_block_cap);
    print_debug_heap(output, heap_start);

    _free(second_block.block->contents);
    _free(third_block.block->contents);
    print_debug_heap(output, heap_start);
    const struct block_header* first_free_block = first_block.block->next;
    const struct block_header* second_free_block = first_free_block->next;
    assert(first_free_block->is_free);
    assert(first_free_block->capacity.bytes == second_block_cap);
    assert(second_free_block->is_free && second_free_block->next == NULL);

    test_end(output, heap_start);
}

void test_merging_blocks_in_allocation(FILE* output) {
    void* heap_start = test_start("Test: Merging blocks in allocation", output);

    const size_t first_block_cap = 1024;
    const struct test_block first_block = alloc_block(first_block_cap);

    const size_t second_block_cap = 5 * 1024;
    const struct test_block second_block = alloc_block(second_block_cap);

    const size_t third_block_cap = 1000 * 1024;
    const struct test_block third_block = alloc_block(third_block_cap);

    _free(second_block.block->contents);
    _free(third_block.block->contents);
    print_debug_heap(output, heap_start);

    const size_t fourth_block_cap = 10 * 1024;
    const struct test_block fourth_block = alloc_block(fourth_block_cap);
    print_debug_heap(output, heap_start);

    const struct block_header* free_block = fourth_block.block->next;
    assert(fourth_block.block && free_block->next == NULL);
    assert(size_from_capacity(free_block->capacity).bytes == HEAP_DEFAULT_SIZE - first_block.sz.bytes - fourth_block.sz.bytes);

    test_end(output, heap_start);
}

void test_grow_heap_extending(FILE* output) {
    void* heap_start = test_start("Test: Grow heap extending", output);

    const size_t first_block_cap = HEAP_DEFAULT_SIZE - 32;
    const struct test_block first_block = alloc_block(first_block_cap);

    const size_t second_block_cap = 1024;
    const struct test_block second_block = alloc_block(second_block_cap);
    print_debug_heap(output, heap_start);
    assert(second_block.block == first_block.block->next);
    assert((uint8_t*) first_block.block + first_block.sz.bytes == (uint8_t*) second_block.block);

    assert(first_block.block->capacity.bytes + second_block.block->capacity.bytes > HEAP_DEFAULT_SIZE);

    test_end(output, heap_start);
}

void test_grow_heap_not_extending(FILE* output) {
    void* heap_start = test_start("Test: Grow heap not extending", output);

    const size_t first_block_cap = HEAP_DEFAULT_SIZE - 32;
    const struct test_block first_block = alloc_block(first_block_cap);

    const size_t second_block_cap = 1024 * 1024 * 1024;
    const struct test_block second_block = alloc_block(second_block_cap);
    print_debug_heap(output, heap_start);
    assert(second_block.block == first_block.block->next);
	assert((uint8_t*) first_block.block + first_block.sz.bytes != (uint8_t*) second_block.block);

    test_end(output, heap_start);
}
