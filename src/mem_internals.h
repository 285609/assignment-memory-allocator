#ifndef _MEM_INTERNALS_H_
#define _MEM_INTERNALS_H_

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

struct region { 
	void* addr; 
	size_t size; 
	bool extends; 
};

struct block_header {
	struct block_header* next;
	block_capacity capacity;
	bool is_free;
	uint8_t contents[];
};


static const struct region REGION_INVALID = {0};

inline bool region_is_invalid(const struct region* const r) { return r->addr == NULL; }

inline block_size size_from_capacity(const block_capacity cap) { 
	return (block_size) { cap.bytes + offsetof(struct block_header, contents) }; 
}

inline block_capacity capacity_from_size(const block_size sz) { 
	return (block_capacity) { sz.bytes - offsetof(struct block_header, contents) }; 
}

struct block_header* block_get_header(const void* const contents);

#endif
