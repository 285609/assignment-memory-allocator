#include <stdio.h>

#include "test.h"
#include "util.h"

static FILE* open_file_to_write(const char* fname) {
	return fopen(fname, "w");
}

int main(void) {
	const char* fname = "test_memalloc.txt";
	FILE* file = open_file_to_write(fname);
	if (!file) {
		err("Couldn't open file for tests.");
	}
	
	for (size_t i = 0; i < TEST_COUNT; i++) {
        tests[i](file);
    }
	printf("All tests are passed! \nCheck results in %s\n", fname);
	
	return 0;
}